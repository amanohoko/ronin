Environment fundamentals :
==========================

* RONIN_ROOT       : The Ronin's root path.
* NOH_ROOT         : The Ronin's Meta abstraction path.

* RONIN_RECIPE     : The Noh-art's recipes directory.
* RONIN_LANG_SUB   : The Noh-art's programming languages path prefix.

Noh-art plays :
===============

* RONIN_OS_DIR     : Tells where are the OS specs.

* RONIN_HOME_ROOT  : Tells the root's home directory path.
* RONIN_HOME_DIR   : Tells the users home directory path.

* RONIN_CHARMS_DIR : Directory from which we loads IT charms.
* RONIN_SDK_DIR    : Directory from which we loads availible SDKs.

Runtime variables :
===================

* RONIN_OS               : Tells on which operating system the Ronin is running.

Runtime environment :
=====================

* RONIN_SPECS            : Tells the directory of the OS wrapper within Ronin.

* RONIN_SPECS_NAME       : OS release -> Distribution name.
* RONIN_SPECS_ID         : OS release -> Distribution ID.
* RONIN_SPECS_VERSION    : OS release -> Distribution version.
* RONIN_SPECS_ANSI_COLOR : OS release -> ANSI color support.

* RONIN_LANG_DIR         : The Ronin's Meta abstraction path.
* RONIN_COOKBOOK         : Directory from which we loads availible recipes.

* RONIN_BOOT             : Tells on which operating system the Ronin is running.
* RONIN_PATH             : The Ronin's system path for executables.

Deep integration :
==================

# OS :
------

* OS_NAME          : An OS name.
* OS_PATH          : An OS name.

# Langague :
------------

* LANG_NAME        : A programming language name.
* LANG_PATH        : A programming language root directory.
* LANG_INCLUDES    : A programming language include paths.

# SDK :
-------

* SDK_NAME         : An SDK name.
* SDK_PATH         : An SDK name.

# Charms :
----------

* CHARM_NAME       : A charm name
* CHARM_PATH       : A charm name.

# Recipes :
-----------

* RECIPE_NAME      : A recipe name.
* RECIPE_PATH      : A recipe name.

Shell aliases :
===============

# Shell common helpers

alias random='od -vAn -N4 -tu4 < /dev/urandom'
alias ssh-clear='ssh-keygen -f $HOME/.ssh/known_hosts -R '
alias pwdgen='< /dev/urandom tr -dc 'A-Za-z0-9\\?!=-_' | head -c13'

# Shell listing enhancement

alias lh='ll -h'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias lsize='ls -l $1 | wc -l'

# POSIX enhancement

alias reboot="sudo shutdown -r now"
alias :q="exit"

# Supervisor's shortcuts

alias superctl='supervisorctl'
alias superstart='supervisorctl start'
alias superrestart='supervisorctl restart'
alias superstop='supervisorctl stop'
alias supertail='supervisorctl tail -f'

Shell extensibility :
=====================

$HOME/.shelter/env.sh
