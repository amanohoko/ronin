#!/bin/sh

for pth in $LANG_INCLUDES ; do
  export JAVA_PATH=$JAVA_PATH":"$pth
done

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(ant grails mvn)
fi
