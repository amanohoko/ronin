#!/bin/sh

case $SHL_OS in
  windows)
    export CHOCO_PKGs=$CHOCO_PKGs" ruby1.9 rubygems"
    ;;
  debian|ubuntu|raspbian)
    export APT_PKGs=$APT_PKGs" ruby1.9.3 rubygems"
    ;;
  macosx)
    #export BREW_PKGs=$BREW_PKGs" nodejs"
    ;;
esac
