#!/bin/sh

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(ruby gem rvm rails rake)
fi

#*******************************************************************************

export RUBY_PKGs="foreman"
export RUBY_PKGs="$RUBY_PKGs fpm god bluepill"
export RUBY_PKGs="$RUBY_PKGs dydra"
export RUBY_PKGs="$RUBY_PKGs rhc heroku af"
export RUBY_PKGs="$RUBY_PKGs cloudapp_api"

alias ruby-install="gem install --no-rdoc --no-ri"
