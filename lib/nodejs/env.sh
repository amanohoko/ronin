#!/bin/sh

for pth in $LANG_INCLUDES ; do
  export NODE_PATH=$NODE_PATH":"$pth
done

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(coffee cake)
  ZSH_PLUGINs+=(node npm meteor)
  ZSH_PLUGINs+=(bower)
fi

#*******************************************************************************

export NPM_PKGs="less"

alias nodejs-install="npm install -g"
