#-*- coding: utf-8 -*-

from shelter.contrib.saloon.shortcuts import *

@Saloon.register('mpd')
class MusicPlayerDaemon(Saloon.Backend):
    def configure(self):
        self.render('saloon/mpd/daemon.conf', self.rpath('etc/mpd.conf'))
    
    def run(self):
        Nucleon.local.shell('/usr/bin/mpd', '--no-daemon', self.rpath('etc', 'mpd.conf'))

