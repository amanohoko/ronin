#-*- coding: utf-8 -*-

from shelter.contrib.saloon.shortcuts import *

@Saloon.register('transmission')
class TransmissionDaemon(Saloon.Backend):
    def configure(self):
        self.render('saloon/mpd/daemon.conf', self.rpath('etc/mpd.conf'))
        
        resp = json.loads(open('/hml/etc/saloon/transmission.json').read())
        
        resp['download-dir']   = self.context['transmission']['finished']
        resp['incomplete-dir'] = self.context['transmission']['incomplete']
        resp['peer-port']      = self.context['ports']['p2p_peer']
        resp['rpc-port']       = self.context['ports']['p2p_rpc']
        resp['rpc-username']   = self.context['creds']['user']
        resp['rpc-password']   = self.context['creds']['passwd']
        
        Nucleon.write(self.bpath('.config', 'transmission-daemon', 'settings.json'), json.dumps(resp, indent=4 * ' '))
    
    def run(self):
        Nucleon.local.shell('/usr/bin/transmission-daemon', '-f')

