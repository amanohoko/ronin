#-*- coding: utf-8 -*-

from shelter.contrib.saloon.shortcuts import *

@Saloon.register('vlc')
class VlcPlayer(Saloon.Backend):
    def configure(self):
        pass
    
    def run(self):
        Nucleon.local.process('/usr/bin/vlc', '-I', 'http',
            '--http-host', self.context['listen_on'],
            '--http-port', str(self.context['ports']['vlc_rpc']),
        wait=False)

