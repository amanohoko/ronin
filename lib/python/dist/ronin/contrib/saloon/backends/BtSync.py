#-*- coding: utf-8 -*-

from shelter.contrib.saloon.shortcuts import *

@Saloon.register('btsync')
class BitTorrentSync(Saloon.Backend):
    def configure(self):
        Nucleon.write(self.rpath('etc', 'btsync.conf'), json.dumps({ 
            "device_name":   "Virgil's Shares",
            "listening_port" : self.context['ports']['bts_peer'],
            "storage_path" : self.rpath('var', 'btsync'),
            #"pid_file" : self.rpath('run', 'btsync.pid'),
            "check_for_updates" : False, 
            "use_upnp" : True,
            "download_limit" : 0,                       
            "upload_limit" : 0, 
            "webui": {
                "listen" : "0.0.0.0:%(bts_adm)d" % self.context['ports'],
                "login" : self.context['creds']['user'],
                "password" : self.context['creds']['passwd'],
            },
            "shared_folders" : [{
                "secret" : "A33KDOETWE7WZP6W6L25VZRSIJ7NYYUPZ",
                "dir" : "/zen/media/shares/eXchange",
                "use_relay_server" : True,
                "use_tracker" : True, 
                "use_dht" : False,
                "search_lan" : True,
                "use_sync_trash" : True,
                "known_hosts" : [
                    #"192.168.1.2:44444",
                ],
            }],
        }, indent=4 * ' '))
    
    def run(self):
        Nucleon.local.shell('/hml/bin/btsync', '--nodaemon', '--config', self.rpath('etc', 'btsync.conf'))

