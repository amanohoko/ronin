#-*- coding: utf-8 -*-

from shelter.contrib.saloon.shortcuts import *

@Saloon.register('xbmc')
class XbmcInstance(Saloon.Backend):
    @property
    def context(self):
        return {}
    
    def configure(self):
        pass
    
    def run(self):
        Nucleon.local.shell('xbmc')

