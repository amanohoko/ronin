class Saloon(object):
    def __init__(self):
        self._reg = {}
    
    def register(self, *args, **kwargs):
        def do_apply(handler, key):
            if key not in self._reg:
                self._reg[key] = handler
            
            return handler
        
        return lambda hnd: do_apply(hnd, *args, **kwargs)
    
    ################################################################################"
    
    
    
    ################################################################################"
    
    class Backend(object):
        def __init__(self, mgr, user, key):
            self._mgr = mgr
            self._usr = user
        
        manager = property(lambda self: self._mgr)
        user    = property(lambda self: self._usr)
        
        context = property(lambda self: self.manager.context)
        
        def render(self, *args, **kwargs):
            kwargs.update(self.manager.context)
            
            Nucleon.render(*args, **kwargs)

Saloon = Saloon()

