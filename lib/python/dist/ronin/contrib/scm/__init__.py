#-*- coding: utf-8 -*-

from python2use.core.helpers import *
from python2use.core.abstract import *
from python2use.core.utils import Nucleon

class Repository(object):
    def __init__(self, path, parent=None):
        self._pth = path
        
        self._prn = parent
        
        self.reset()
    
    def reset(self):
        try:
            self._git = git.Repo(self.path)
        except: # git.errors.NoSuchPathError,ex:
            self._git = None
    
    parent = property(lambda self: self._prn)
    path   = property(lambda self: self._pth)
    repo   = property(lambda self: self._git)
    
    meta   = property(lambda self: self.parent)
    
    ############################################################################
    
    def exists(self, *args, **kwargs): return Nucleon.local.exists(self.path, *args, **kwargs)
    def chdir(self, *args, **kwargs):  return Nucleon.local.chdir(self.path, *args, **kwargs)
    def shell(self, *args, **kwargs):  return Nucleon.local.shell(*args, **kwargs)
    
    ############################################################################
    
    @property
    def branches(self):
        return ['master']
    
    @property
    def remotes(self):
        #for rmt in Nucleon.local.execute('git', 'remote', sep=[' ', "\t", "\n"]):
        return ['origin']
    
    #***************************************************************************
    
    @property
    def dirty(self):
        try:
            return self.repo.is_dirty
        except:
            return False
    
    @property
    def stale(self):
        try:
            return self.repo.is_stale
        except:
            return False
    
    #***************************************************************************
    
    def pull(self, brn, *remotes):
        for rmt in remotes:
            Nucleon.local.shell('git', 'pull', rmt, brn)
    
    def push(self, brn, *remotes):
        for rmt in remotes:
            Nucleon.local.shell('git', 'push', rmt, brn)
    
    def submodules(self):
        flags = ['--init']
        
        if False:
            flags += ['--recursive']
        
        Nucleon.local.shell('git', 'submodule', 'update', *flags)
    
    ############################################################################
    
    @property
    def flavor(self):
        if not hasattr(self, '_flv'):
            import python2use.contrib.hosting.flavors
            
            resp = Flavor.provision(self)
            
            setattr(self, '_flv', resp)
        
        return self._flv
    
    def dynamic_recipe(self, svc):
        nrw = 'config_%s' % svc.name
        
        if self.flavor is not None:
            hnd = getattr(self.flavor, nrw, None)
            
            if callable(hnd):
                return "\n".join(list(hnd(svc)))
        
        return ""

