#-*- coding: utf-8 -*-

from python2use.shortcuts import *

@Daemon.register
class Redis(Daemon):
    class Instance(Backend):
        EP_PORT = 'tcp'
        
        @property
        def daemon_call(self):
            yield '/usr/bin/redis-server'
            
            yield self.cfg_path
        
        def parameters(self, cfg):
            cfg['max_dbs'] = 8
            cfg['timeout'] = 300
        
        def setup(self):
            pass

@Daemon.register
class MongoDB(Daemon):
    class Instance(Backend):
        EP_PORT = 'tcp'
        
        @property
        def daemon_call(self):
            yield '/usr/bin/mongod'
            yield '-f'
            yield self.cfg_path
            yield '--ipv6'
            yield '--rest'
            yield '--jsonp'
            yield '--directoryperdb'
        
        def parameters(self, cfg):
            pass
        
        def setup(self):
            pass

@Daemon.register
class ElasticSearch(Daemon):
    class Instance(Backend):
        EP_PORT = ''
        
        @property
        def daemon_call(self):
            yield '/shl/bin/elastic_node'
            yield self.cfg_path
            yield self.ctl_path
        
        def parameters(self, cfg):
            cfg['shards']     = 5
            cfg['replicas']   = 3
            cfg['max_length'] = '100mb'
        
        def setup(self):
            pass

@Daemon.register
class CouchDB(Daemon):
    class Instance(Backend):
        EP_PORT = 'http'
        
        @property
        def daemon_call(self):
            yield '/usr/bin/couchdb'
            yield '-a'
            yield self.cfg_path
            yield '-r'
            yield '5'
            yield '-p'
            yield self.rpath('daemon.pid')
        
        def parameters(self, cfg):
            cfg['compression']    = 8
            cfg['max_doc_size']   = 4294967296
            cfg['max_chunk_size'] = 4294967296
            cfg['os_timeout']     = 5000
        
        def setup(self):
            pass

@Daemon.register
class MariaDB(Daemon):
    class Instance(Backend):
        EP_PORT = 'tcp'
        
        @property
        def daemon_call(self):
            yield 'mysqld'
            yield '--defaults-file=%s' % self.cfg_path
        
        def parameters(self, cfg):
            cfg['databases'] = 8
        
        def setup(self):
            pass

