#-*- coding: utf-8 -*-

from python2use.shortcuts import *

class NetDaemon(Daemon):
    TPL_ROOT = 'network'

@Daemon.register
class HostNet(NetDaemon):
    @property
    def context(self):
        return dict(
            hostname = Nucleon.local.hostname,
            hostfqdn = Nucleon.local.hostfqdn,
            
            ifaces = [
                dict(iface='lo', inet='loopback', config={}),
                dict(iface='eth0', inet='static', config={
                    'address':   '192.168.1.253',
                    'netmask':   '255.255.255.0',
                    'gateway':   '192.168.1.1',
                }),
                dict(iface='wlan0', inet='static', config={
                    'address':   '10.7.0.1',
                    'netmask':   '255.255.255.0',
                    'broadcast': '10.7.0.255',
                }),
                dict(iface='wlan0:0', inet='static', config={
                    'address':   '10.7.0.253',
                    'netmask':   '255.255.255.0',
                    'broadcast': '10.7.0.255',
                }),
            ],
        )
    
    def config_daemon(self, **cnt):
        self.render_tpl('interfaces',    '/etc/network/interfaces', **cnt)
        self.render_tpl('hosts',         '/etc/hosts',              **cnt)
        self.render_tpl('hosts.allow',   '/etc/hosts.allow',        **cnt)
        self.render_tpl('host.conf',     '/etc/host.conf',          **cnt)
        self.render_tpl('insserv.conf',  '/etc/insserv.conf',       **cnt)
        self.render_tpl('nsswitch.conf', '/etc/nsswitch.conf',      **cnt)

@Daemon.register
class HostAPd(NetDaemon):
    def config_daemon(self, **cnt):
        pass # self.render_tpl('hostapd.conf', '/etc/hostapd/byod.conf',      **cnt)

@Daemon.register
class DHCP(NetDaemon):
    SERVICEs = ['isc-dhcp-server', 'isc-dhcp-server6']
    
    def config_daemon(self, **cnt):
        pass # self.render_tpl('wifi/dhcpd.conf', '/etc/hostapd/byod.conf',      **cnt)

@Daemon.register
class NTP(NetDaemon):
    def config_daemon(self, **cnt):
        self.render_tpl('ntp.conf', '/etc/ntp.conf', **cnt)

@Daemon.register
class Tor(NetDaemon):
    def config_daemon(self, **cnt):
        self.render_tpl('tor.rc', '/etc/tor/torrc', **cnt)

@Daemon.register
class Miredo(NetDaemon):
    def config_daemon(self, **cnt):
        self.render_tpl('miredo.conf', '/etc/miredo.conf', **cnt)

