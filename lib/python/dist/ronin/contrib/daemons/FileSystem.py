#-*- coding: utf-8 -*-

from python2use.shortcuts import *

class FileDaemon(Daemon):
    TPL_ROOT = 'saloon'

@Daemon.register
class FsTab(FileDaemon):
    def config_daemon(self, **cnt):
        self.render_tpl('fstab', '/etc/fstab', **cnt)
    
    def control(self, *args, **kwarg):
        pass

@Daemon.register
class NFS(FileDaemon):
    SERVICEs = ['nfs-kernel-server']
    
    def config_daemon(self, **cnt):
        self.render_tpl('exports', '/etc/exports', **cnt)

@Daemon.register
class Samba(FileDaemon):
    SERVICEs = ['nmbd','smbd']
    
    def config_daemon(self, **cnt):
        pth = '/shl/var/lib/supervisor/cluster'
        
        if not Nucleon.local.exists(pth):
            Nucleon.local.shell('mkdir', '-p', pth)
        
        self.render('daemon.conf', '/etc/samba/smb.conf',
            bpath = pth,
        **cnt)

