#-*- coding: utf-8 -*-

from python2use.shortcuts import *

@Daemon.register
class Bind9(ClusterDaemon):
    PUBLIC_DNS = {
        'google-ns1': '8.8.8.8',
        'google-ns2': '8.8.4.4',
    }
    
    SPARSE       = 'domain'
    
    CFG_DEPLOY   = property(lambda self: '/etc/bind/deploy.d')
    CFG_TARGET   = property(lambda self: '/etc/bind/discover.d')
    CFG_REGEX    = re.compile('vhost-(.+)')
    CFG_LOCATION = 'vhost-%(fqdn)s'
    
    def config_daemon(self, **cnt):
        self.render('daemon.conf', '/etc/bind/named.conf',
            dns_forward = self.PUBLIC_DNS.values(),
            endpoint=dict(
                ipv4 = [],
                ipv6 = [],
            ),
        **cnt)
    
    def config_vhost(self, vhost, **cnt):
        pass
    
    def config_domain(self, rfl, **cnt):
        self.render('domain.conf', '/etc/bind/named.conf',
            reflector = rfl,
            vhost     = rfl.vhost,
            fqdn      = rfl.fqdn,
            endpoint=dict(
                ipv4 = [],
                ipv6 = [],
            ),
        **cnt)

@Daemon.register
class Avahi(Daemon):
    TPL_ROOT = 'network'
    
    def config_daemon(self, **cnt):
        self.render('daemon.conf', '/etc/avahi/avahi-daemon.conf',
            endpoint=dict(
                ipv4 = [],
                ipv6 = [],
            ),
        **cnt)

