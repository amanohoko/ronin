#-*- coding: utf-8 -*-

from python2use.shortcuts import *

class ShieldPiece(Daemon):
    TPL_ROOT = 'shield'

@Daemon.register
class UFW(ShieldPiece):
    def config_daemon(self, **cnt):
        self.render('ufw.conf',           '/etc/ufw/ufw.conf',       **cnt)
        self.render('sysctl.conf',        '/etc/ufw/sysctl.conf',    **cnt)
        
        for key in ('before','before6','after','after6'):
            self.render('%s.rules' % key, '/etc/ufw/%s.rules' % key, **cnt)
    
    def control(self, *args, **kwarg):
        Nucleon.local.shell('ufw', 'reload')

@Daemon.register
class PSAd(ShieldPiece):
    def config_daemon(self, **cnt):
        self.render('auto_dl',          '/etc/psad/auto_dl',       **cnt)
        self.render('icmp_types',       '/etc/psad/icmp_types',    **cnt)
        self.render('ip_options',       '/etc/psad/ip_options',    **cnt)
        self.render('pf.os',            '/etc/psad/pf.os',         **cnt)
        self.render('posf',             '/etc/psad/posf',          **cnt)
        self.render('psad.conf',        '/etc/psad/psad.conf',     **cnt)
        self.render('signatures',       '/etc/psad/signatures',    **cnt)
        self.render('snort_rule_dl',    '/etc/psad/snort_rule_dl', **cnt)

@Daemon.register
class ClamAV(ShieldPiece):
    SERVICEs = ['clamav-daemon','clamav-freshclam']
    
    def config_daemon(self, **cnt):
        self.render('clamd.conf',     '/etc/clamav/clamd.conf',     **cnt)
        self.render('freshclam.conf', '/etc/clamav/freshclam.conf', **cnt)

