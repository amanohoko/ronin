#-*- coding: utf-8 -*-

from python2use.shortcuts import *

class SaloonDaemon(Daemon):
    TPL_ROOT = 'saloon'
    
    def render(self, *args, **kwargs):
        
        return super(SaloonDaemon, self).render(*args, **kwargs)

@Daemon.register
class MPD(SaloonDaemon):
    def config_daemon(self, **cnt):
        self.render('daemon.conf', '/etc/mpd.conf',
            jukebox_path = '/hml/media/files/JukeBox',
            varpath      = lambda *x: os.path.join('/hml', 'media', 'var', 'mpd', *x),
            ports        = dict(
                mpd = 6600,
            ),
            creds       = dict(
                user   = 'jevad',
                passwd = 'Kaliamintra',
            ),
        **cnt)

@Daemon.register
class Transmission(SaloonDaemon):
    def config_daemon(self, **cnt):
        Nucleon.local.write_json('/etc/transmission-daemon/settings.json', {
            "alt-speed-down": 50, 
            "alt-speed-enabled": False, 
            "alt-speed-time-begin": 540, 
            "alt-speed-time-day": 127, 
            "alt-speed-time-enabled": False, 
            "alt-speed-time-end": 1020, 
            "alt-speed-up": 50, 
            "bind-address-ipv4": "0.0.0.0", 
            "bind-address-ipv6": "::", 
            "blocklist-enabled": False, 
            "blocklist-url": "http://www.example.com/blocklist", 
            "cache-size-mb": 4, 
            "dht-enabled": True, 
            "download-dir": "/hml/media/var/transmission/Finished", 
            "download-limit": 100, 
            "download-limit-enabled": 0, 
            "download-queue-enabled": True, 
            "download-queue-size": 5, 
            "encryption": 1, 
            "idle-seeding-limit": 30, 
            "idle-seeding-limit-enabled": False, 
            "incomplete-dir": "/hml/media/var/transmission/Incomplete", 
            "incomplete-dir-enabled": True, 
            "lpd-enabled": False, 
            "max-peers-global": 200, 
            "message-level": 2, 
            "peer-congestion-algorithm": "", 
            "peer-limit-global": 240, 
            "peer-limit-per-torrent": 60, 
            "peer-port": 51413, 
            "peer-port-random-high": 65535, 
            "peer-port-random-low": 49152, 
            "peer-port-random-on-start": False, 
            "peer-socket-tos": "default", 
            "pex-enabled": True, 
            "port-forwarding-enabled": False, 
            "preallocation": 1, 
            "prefetch-enabled": 1, 
            "queue-stalled-enabled": True, 
            "queue-stalled-minutes": 30, 
            "ratio-limit": 0.2000, 
            "ratio-limit-enabled": False, 
            "rename-partial-files": True, 
            "rpc-authentication-required": True, 
            "rpc-bind-address": "0.0.0.0", 
            "rpc-enabled": True, 
            "rpc-password": "{444435a6ef3032fcb4411097610d2cd68c59c19ezV2rOr/E", 
            "rpc-port": 9091, 
            "rpc-url": "/transmission/", 
            "rpc-username": "transmission", 
            "rpc-whitelist": "127.0.0.1", 
            "rpc-whitelist-enabled": False, 
            "scrape-paused-torrents-enabled": True, 
            "script-torrent-done-enabled": False, 
            "script-torrent-done-filename": "", 
            "seed-queue-enabled": True, 
            "seed-queue-size": 2, 
            "speed-limit-down": 100, 
            "speed-limit-down-enabled": False, 
            "speed-limit-up": 10, 
            "speed-limit-up-enabled": True, 
            "start-added-torrents": True, 
            "trash-original-torrent-files": False, 
            "umask": 18, 
            "upload-limit": 30, 
            "upload-limit-enabled": 1, 
            "upload-slots-per-torrent": 20, 
            "utp-enabled": True
        })
