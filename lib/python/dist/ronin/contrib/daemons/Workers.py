#-*- coding: utf-8 -*-

from python2use.shortcuts import *

@Daemon.register
class Supervisor(ClusterDaemon):
    CFG_DEPLOY   = property(lambda self: '/etc/%s/conf-enabled' % self.name)
    CFG_TARGET   = property(lambda self: '/etc/%s/conf-available' % self.name)
    CFG_REGEX = re.compile('vhost-(.+)-(.+)\.conf')
    CFG_LOCATION = 'vhost-%(owner)s-%(name)s.conf'
    
    def config_daemon(self, **cnt):
        cnt.update(self.context)
        
        pth = '/shl/var/lib/supervisor/cluster'
        
        if not Nucleon.local.exists(pth):
            Nucleon.local.shell('mkdir', '-p', pth)
        
        self.render('daemon.conf', '/etc/supervisor/supervisord.conf',
            bpath = pth,
            incls = '/etc/supervisor/conf-enabled/*.conf /shl/etc/cluster/supervisor/*.conf /hml/etc/cluster/supervisor/*.conf',
        **cnt)
    
    def control(self, acte):
        if acte=='start':
            Nucleon.local.shell('supervisord')
        elif acte=='stop':
            Nucleon.local.shell('supervisorctl', 'shutdown')
        elif acte=='restart':
            self.control('stop')
            self.control('start')
        else:
            Nucleon.local.shell('supervisorctl', acte)

