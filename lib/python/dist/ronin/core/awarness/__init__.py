#-*- coding: utf-8 -*-

from python2use.shortcuts import *

from shelter.utils import *

################################################################################

class Broadcaster(object):
    parent  = property(lambda self: self._prn)
    name    = property(lambda self: self._key)
    stype   = property(lambda self: self._type)
    
    domain  = property(lambda self: self._dns)
    host    = property(lambda self: self._host)
    port    = property(lambda self: self._port)
    
    text    = property(lambda self: self._text)
    
    meta    = property(lambda self: self.parent)
    
    ############################################################################
    
    def __init__(self, parent, name, port, stype="_http._tcp", domain="", host="", text=""):
        self._prn = parent
        self._key = name
        self._type = stype
        self._dns = domain
        self._host = host
        self._port = port
        self._text = text

        self.reset()
    
    ############################################################################
    
    def rpath(self, *args, **kwargs): return Nucleon.local.join(self.path, *args, **kwargs)
    def exists(self, *args, **kwargs): return Nucleon.local.exists(self.rpath(*args), **kwargs)
    def chdir(self, *args, **kwargs):  return Nucleon.local.chdir(self.rpath(*args), **kwargs)
    def shell(self, *args, **kwargs):  return Nucleon.local.shell(*args, **kwargs)
    
    ############################################################################
    
    def reset(self):
        try:
            self.publish()
        except:
            pass
        
        try:
            self.unpublish()
        except:
            pass
    
    class Discovery(object):
        def __init__(self, *args, **kwargs):
            self._reg = []
            
            self.initialize(*args, **kwargs)
        
        def append(self, narrow, **entry):
            self._reg += [entry]
            
            return entry
        
        def __iter__(self): return self._reg.__iter__()

################################################################################

import bluetooth._bluetooth as bluez

from . import BLUEz

class Bluez(Broadcaster):
    class Discover(Broadcaster.Discovery):
        def initialize(self, device=0):
            self._dev = device
        
        device = property(lambda self: self._dev)
        
        def scan(self):
            try:
                self.sock = bluez.hci_open_dev(self.device)
                print "ble thread started"
            except:
                print "error accessing bluetooth device..."
                sys.exit(1)
            
            BLUEz.hci_le_set_scan_parameters(self.sock)
            BLUEz.hci_enable_le_scan(self.sock)
            
            while True:
                lst = BLUEz.parse_events(self.sock, 10)
                
                for beacon in lst:
                    self.handler(beacon)
        
        def handler(self, beacon):
            self.append(self.device, beacon)

################################################################################

import dbus, gobject, avahi

from dbus import DBusException
from dbus.mainloop.glib import DBusGMainLoop

class Zeroconf(Broadcaster):
    class Discover(Broadcaster.Discovery):
        def initialize(self, lookup=['_daap._tcp'], **config):
            self._lkp = lookup
            self._cfg = config
        
        lookup = property(lambda self: self._lkp)
        config = property(lambda self: self._cfg)
        
        def service_resolved(self, *args):
            print 'service resolved'
            print 'name:', args[2]
            print 'address:', args[7]
            print 'port:', args[8]
        
        def print_error(self, *args):
            print 'error_handler'
            print args[0]
        
        def scan(self):
            self.loop = DBusGMainLoop()
            
            self.bus = dbus.SystemBus(mainloop=self.loop)
            
            self.server = dbus.Interface(self.bus.get_object(avahi.DBUS_NAME, '/'), 'org.freedesktop.Avahi.Server')
            
            for TYPE in self.lookup:
                self.browser = dbus.Interface(
                    self.bus.get_object(avahi.DBUS_NAME,
                        self.server.ServiceBrowserNew(
                            avahi.IF_UNSPEC,
                            avahi.PROTO_UNSPEC,
                            TYPE,
                            'local',
                            dbus.UInt32(0)
                        )
                    ),
                    avahi.DBUS_INTERFACE_SERVICE_BROWSER
                )
                
                self.browser.connect_to_signal("ItemNew", self.handler)
            
            gobject.MainLoop().run()
        
        def handler(self, interface, protocol, name, stype, domain, flags):
            print "Found service '%s' type '%s' domain '%s' " % (name, stype, domain)
            
            if flags & avahi.LOOKUP_RESULT_LOCAL:
                # local service, skip
                pass
            
            self.server.ResolveService(interface, protocol, name, stype, 
                domain, avahi.PROTO_UNSPEC, dbus.UInt32(0), 
                reply_handler=service_resolved, error_handler=print_error
            )
            
            self.append(lookup, dict(iface=interface, proto=protocol, name=name, stype=stype, domain=domain, flags=flags))
    
    def publish(self):
        bus = dbus.SystemBus()
        server = dbus.Interface(
                         bus.get_object(
                                 avahi.DBUS_NAME,
                                 avahi.DBUS_PATH_SERVER),
                        avahi.DBUS_INTERFACE_SERVER)
        
        g = dbus.Interface(
                    bus.get_object(avahi.DBUS_NAME,
                                   server.EntryGroupNew()),
                    avahi.DBUS_INTERFACE_ENTRY_GROUP)
        
        g.AddService(avahi.IF_UNSPEC, avahi.PROTO_UNSPEC,dbus.UInt32(0),
                     self.name, self.stype, self.domain, self.host,
                     dbus.UInt16(self.port), self.text)
        
        g.Commit()
        self.group = g
    
    def unpublish(self):
        self.group.Reset()
