#-*- coding: utf-8 -*-

from python2use.shortcuts import *

from shelter.utils import *

import git

class Repository(object):
    def __init__(self, path, parent=None):
        self._pth = path
        
        self._prn = parent
        
        self.reset()
    
    def reset(self):
        try:
            self._git = git.Repo(self.path)
        except:
            self._git = None
        
        self._flv = None
    
    parent = property(lambda self: self._prn)
    path   = property(lambda self: self._pth)
    repo   = property(lambda self: self._git)
    
    meta   = property(lambda self: self.parent)
    
    ############################################################################
    
    def rpath(self, *args, **kwargs): return Nucleon.local.join(self.path, *args, **kwargs)
    def exists(self, *args, **kwargs): return Nucleon.local.exists(self.rpath(*args), **kwargs)
    def chdir(self, *args, **kwargs):  return Nucleon.local.chdir(self.rpath(*args), **kwargs)
    def shell(self, *args, **kwargs):  return Nucleon.local.shell(*args, **kwargs)
    
    ############################################################################
    
    @property
    def branches(self):
        try:
            return [b.name for b in self.repo.branches]
        except:
            return []
    
    @property
    def remotes(self):
        try:
            return [r.name for r in self.repo.remotes]
        except:
            return []
    
    @property
    def remote_links(self):
        if self.repo:
            try:
                return dict([(r.name, r.url) for r in self.repo.remotes])
            except:
                pass
        
        return {}
    
    #***************************************************************************
    
    @property
    def bare(self):
        try:
            return self.repo.bare
        except:
            return False
    
    @property
    def dirty(self):
        try:
            return self.repo.is_dirty()
        except:
            return False
    
    @property
    def stale(self):
        try:
            return self.repo.is_stale
        except:
            return False
    
    #***************************************************************************
    
    def pull(self, brn, *remotes):
        for rmt in remotes:
            Nucleon.local.shell('git', 'pull', rmt, brn)
    
    def push(self, brn, *remotes):
        for rmt in remotes:
            Nucleon.local.shell('git', 'push', rmt, brn)
    
    def submodules(self):
        flags = ['--init']
        
        if False:
            flags += ['--recursive']
        
        Nucleon.local.shell('git', 'submodule', 'update', *flags)
    
    ############################################################################
    
    @property
    def flavor(self):
        if self._flv==None:
            from voodoo.shortcuts import Flavor

            import voodoo.ext.flavors
            
            self._flv = Flavor.provision(self)
        
        return self._flv
    
    def dynamic_recipe(self, svc):
        nrw = 'config_%s' % svc.name
        
        if self.flavor is not None:
            hnd = getattr(self.flavor, nrw, None)
            
            if callable(hnd):
                return "\n".join(list(hnd(svc)))
        
        return ""

