#!/bin/sh

case $SHL_OS in
  windows)
    export CHOCO_PKGs=$CHOCO_PKGs" python"
    export CYG_PKGs=$CYG_PKGs",python-argparse,python-avahi,python-lxml"
    ;;
  debian|ubuntu|raspbian)
    export APT_PKGs="$APT_PKGs pypy `echo python-{setuptools,software-properties,virtualenv}`"
    ;;
  macosx)
    #export BREW_PKGs=$BREW_PKGs" nodejs"
    ;;
esac
