#!/bin/sh

export PHP_PATH=$SHL_ROOT"/lib/php/dist:"$SHL_ROOT"/lib/php/dist"

for pth in $LANG_INCLUDES ; do
  export PHP_PATH=$PHP_PATH":"$pth
done

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(composer laravel4 symfony2 yii yii2)
fi

#*******************************************************************************

export PHP_PKGs="mongo"

alias php-install="pecl install"
