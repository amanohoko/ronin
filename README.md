Preparing the system :
===========================

Depending on your operating system, follow the steps defined in the links below :

* [RedHat](https://bitbucket.org/os2use/redhat)

* [Debian](https://bitbucket.org/os2use/debian)
* [Ubuntu 14.04+](https://bitbucket.org/os2use/ubuntu)

For embedded systems, you can check :

* [Raspbian](https://bitbucket.org/os2use/raspbian)
* [Snappy (Ubuntu Core)](https://bitbucket.org/os2use/snappy)

For compliancy-testing purposes, you can check :

* [Mac OS X](https://bitbucket.org/os2use/macosx)
* [Windows 7+](https://bitbucket.org/os2use/windows)

Installing the Ronin :
======================

Run the following commands :

```shell
git clone git@bitbucket.org:it2use/ronin.git  /ron

cd /ron

git submodule update --init --recursive

if [[ ! -d /ron/usr ]] ; then
    git clone https://bitbucket.org/it2use/noh-art.git /ron/usr

    cd /ron/usr

    git submodule update --init --recursive

    git remote rm origin
fi
```

Then, using BASH :

```bash
source boot/shell/bash/env.sh

/ron/boot/strap
```

OR, for the ZSH lovers :

```zsh
source boot/shell/zsh/env.sh

/ron/boot/strap
```

Respecting the Bushido :
========================

First, define your bushido by following the wizard after executing  :

* ronin init

For more shell integration, check [this link](https://bitbucket.org/it2use/ronin/src/HEAD/boot/shell/).

Becoming Noh artist :
=====================

Proceed by setting up your personna within [Noh art](https://bitbucket.org/it2use/noh-art) specs, then fall down the rabbit hole :
s
* noh help

Wandering :
===========

```shell
shelter help
```

Enjoy hacking your potential !-)
